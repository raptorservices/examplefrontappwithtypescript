import {OrderType} from "@/types/orderTypes";

export default async function sortAAList<T>(p_table:T, p_field_sort:any, p_type_of_sorting :OrderType) {
    var tmp=JSON.parse(JSON.stringify(p_table));
    var result:T;
    result=p_table;
    let toDate=(dateStr:string):any=>{
        const [day, month, year] = dateStr.split("-")
        return new Date(Number(year), Number(month) - 1, Number(day));
    };
    switch(p_type_of_sorting){
        case 'asc':
            tmp.sort((a:any,b:any) :number=>{
                let dStart = toDate(a[p_field_sort]);
                let dEnd = toDate(b[p_field_sort]);
                return dStart-dEnd;
            });
            result=JSON.parse(JSON.stringify(tmp));
            break;
        case 'desc':
            tmp.sort((a:any,b:any) :number=>{
                let dStart = toDate(a[p_field_sort]);
                let dEnd = toDate(b[p_field_sort]);
                return dEnd-dStart;
            });
            result=JSON.parse(JSON.stringify(tmp));
            break;
        default:break;
    }
    return result;
}
