import { store } from './store'
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { FieldTest } from '@/types/field'

@Module({ namespaced: true, dynamic: true, store, name: 'storeTimeAndPercentage' })
export default class AppStore2 extends VuexModule {
    Field1:FieldTest = {
        fieldId:"",
        name:"",
        dataTypeName:"",
    }

    @Mutation
    SET_TIME_AND_PERCENTAGE(timeAndPercentageInfo: FieldTest) {
        this.Field1 = timeAndPercentageInfo;
    }

    @Action
    set_time_and_percentage(timeAndPercentageInfo: FieldTest) {
        this.context.commit('SET_TIME_AND_PERCENTAGE', timeAndPercentageInfo);
    }

}
