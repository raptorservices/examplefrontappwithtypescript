import Vue from 'vue';
import Component from 'vue-class-component';
import OneComplicatedView1 from '@/views/OneComplexView1/OneComplexView1';
import OneComplicatedView2 from '@/views/OneComplexView2/OneComplexView2';
import { RAPTOR_ENVIRONMENT } from '@/config/raptor_env';
import { getModule } from 'vuex-module-decorators';
import AppStore1 from '@/store/appStore1';

/*
Interface
Use PascalCase for name.
Use camelCase for members.
 */
interface Example1Test {
    customerName:string,
    dataTypeName:string,
}

interface Example2Test {
    resultName:string,
    dataTypeName:string,
}

@Component({ components: { OneComplicatedView1, OneComplicatedView2 } })
export default class App extends Vue {

    //Use camelCase for variable and function names
    private VarTest1: boolean = false;
    private AppStore1 = getModule(AppStore1);

    FunctWhoDoesTest1 (param1:Example1Test):Example2Test{
        let VarTest:Example2Test={
            resultName:param1.customerName,
            dataTypeName:param1.dataTypeName,
        }
        return VarTest;
    }

    // lifecycle hook
    created(): void {
        //just for example
    }

}
