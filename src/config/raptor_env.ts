const app1UrlPrefix = "/app1/api/test/";
export const RAPTOR_ENVIRONMENT = {
    URL_API: (function () {
        if (location.hostname === "localhost") {
            console.log("Running on local server");
            return "https://cdpbeta.raptorsmartadvisor.com";
            //return "https://cdp.raptorsmartadvisor.com";
            //return "https://raptor-audience-api-dev.azurewebsites.net";
            //return "http://localhost:53852";
            return "http://localhost:5000";
            return "http://localhost:54505";//TODO Gateway Local port change the one above for test with the Audience API
        }

        console.log("Running on Azure");
        return window.location.origin;
    })(),
    URL_Metadata: {
        "getAllStarterMetadata": app1UrlPrefix + "MetaData/",
        "getAllStarterFields": app1UrlPrefix + "DataField/",
        "getAllStarterAttribute": app1UrlPrefix + "CalculatedAttribute/",
        "getAllExportProperties": app1UrlPrefix + "MetaData/",
    },
    URL_User_Login: {
        "login": "/customapi/Login/",
    },
    SAVING_DATA: false,
    DATE_FORMAT: 'DD-MM-YYYY'
};

