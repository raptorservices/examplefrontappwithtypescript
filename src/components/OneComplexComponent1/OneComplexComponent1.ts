import { Vue, Component, Prop } from "vue-property-decorator";
import { getModule } from 'vuex-module-decorators';
import AppStore1 from '@/store/appStore1';
import HelloWorld from '@components/HelloWorld.vue';

interface IstyleOrderingArrows{
    color:string
}

@Component({ components: {  HelloWorld } })
export default class OneComplexComponent1 extends Vue {
    @Prop() customerId!:string;
    tableDataAttributes !:Array<any>;

    private AppStore1 = getModule(AppStore1);


}
