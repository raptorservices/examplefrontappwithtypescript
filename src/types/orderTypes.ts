/*
PascalCase for enum names
 */
export enum OrderType {
    asc = "asc", desc = "desc"
}
