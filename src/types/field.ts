/*
Use PascalCase for name.
Reason: Similar to class

Use camelCase for members.
Reason: Similar to class
 */
export interface FieldTest {
    fieldId:string,
    name:string,
    dataTypeName:string,
}
