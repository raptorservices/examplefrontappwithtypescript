import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/view1',
      name: 'view1',
      component: () => import(/* webpackChunkName: "view1" */ './views/OneComplexView1/OneComplexView1'),
    },
    {
      path: '/view2/:customerId',
      name: 'view2',
      props: true,
      component: () => import(/* webpackChunkName: "view2" */ './views/OneComplexView2/OneComplexView2'),
    },
  ],
});
